<?php
	class ControladorSolicitudes extends ControladorPlantilla
	{
		
			
		public static function insertDatos(){
			if(isset($_POST['sol_ban_id_i'])){ //les coloco i al final para saber que es insercion
				$tecnico = 0;
				$estado = 3;
				$ruta = '';
				if(isset($_FILES['sol_imagen_datos']['tmp_name']) && !empty($_FILES['sol_imagen_datos']['tmp_name']) ){
	                $ruta = self::putImage($_FILES['sol_imagen_datos']['tmp_name'], $_FILES["sol_imagen_datos"]["type"] , __DIR__."/../views/inicidencias_img/", 'views/inicidencias_img/');
	            }

				if(isset($_POST['sol_tec_usu_id_i_i'])){
					$tecnico = $_POST['sol_tec_usu_id_i_i'] ;
					$estado = 4;
				}

				$datos = array(
					'sol_suc_id_i' 					=> $_POST['sol_suc_id_i_i'], 
					'sol_fecha_solicitud_d'  		=> date('Y-m-d'),
					'sol_usu_id_i'					=> $_SESSION['codigo'],
					'sol_requerimiento_t'			=> $_POST['sol_requerimiento_t_i'],
					'sol_tec_usu_id_i'				=> $tecnico,
					'sol_observaciones_t'			=> $_POST['sol_observaciones_t_i'],
					'sol_ban_id_i'					=> $_POST['sol_ban_id_i'],
					'sol_est_id_i'					=> $estado,
					'sol_prio_id'					=> $_POST['sol_prio_id_i'],
					'sol_ruta_evidencia'			=> $ruta,
					'sol_ruta_ot_v'					=> $_POST['sol_ruta_ot_v_i'],
					'sol_id_tps_i'					=> $_POST['sol_id_tps_i_i'],
					'sol_aplica_i'					=> $_POST['sol_aplica_i_i'],
				);

				$respuesta = SolicitudesModelo::insertDatos($datos);
				if($respuesta != "error"){
					
					$orden = SolicitudesModelo::getTotalSolicitudesDay();
					$respuestaX = ModeloAuth:: actualizarUsuarioPostLogin('sc_solicitudes', 'sol_orden_trabajo', date('Ymd').$orden['total'], 'sol_id_i', $respuesta);

					//Aqui vamos a hacer una validacion de si viene o no la observacion
					if(isset($_POST['sol_observaciones_t_i']) && $_POST['sol_observaciones_t_i'] != ''){
						$datos = array (
							'obs_desc_v'      => $_POST['sol_observaciones_t_i'], //observacion
							'obs_usu_id_i'      => $_SESSION['codigo'],//quien hace la observacion
							'obs_sol_id_i'      => $respuesta, //codigo solicitud
							'obs_fecha_d'      => date('Y-m-d H:i:s') //fecha Observacion
						);
						$observacion = SolicitudesModelo::insertObservaciones($datos);	
					}

					if($tecnico != 0){
						$datos = array(
							'asi_usu_tec_id_i' 			=> $tecnico, 
							'asi_fecha_d' 				=> $_POST['sol_fecha_cita_d'],
							'asi_hor_id_i'  			=> $_POST['sol_hora_cita_v'],
							'asi_observacion_v'		    => $_POST['sol_observaciones_t_i'],
							'asi_sol_id_i'				=> $respuesta,
							'asi_fecha_aignacion'		=> date('Y-m-d')
						);
						$respuesta = SolicitudesModelo::insertDatosAsignar($datos);
					}
					
					return json_encode(array('code' => 1, 'message' => 'Solicitud guardadada con exito'));
				}else{	
					return json_encode(array('code' => 0, 'message' => 'Solicitud no guardadado'));
				}
			}
		}


		/**
		*Desc.  => editar un usuario se solicitan lops campos que llegan por metodo post usu_tip_doc_id_i_i, usu_documento_v_i, usu_per_id_i_i, usu_est_id_i_i, usu_banco_i_i, usu_usuario_v_i, usu_password_v_i, usu_id_i
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/	
		public static function UpdateDatos(){
			if(isset($_POST['sol_id_i_e'])){ 
				$tecnico = 0;
				if(isset($_POST['sol_tec_usu_id_i_e'])){
					$tecnico = $_POST['sol_tec_usu_id_i_e'] ;
				}//e de edicion
				$inicidencia = self::getData('sc_solicitudes', 'sol_id_i', $_POST['sol_id_i_e']);
				$estado = $inicidencia['sol_est_id_i'];
				$prioridad = $inicidencia['sol_prio_id'];
				$banco = $inicidencia['sol_ban_id_i'];
				$sucursal = $inicidencia['sol_suc_id_i'];

				$ruta = $inicidencia['sol_ruta_evidencia'];
				if(isset($_FILES['sol_imagen_datos_e']['tmp_name']) && !empty($_FILES['sol_imagen_datos_e']['tmp_name']) ){
	                $ruta = self::putImage($_FILES['sol_imagen_datos_e']['tmp_name'], $_FILES["sol_imagen_datos_e"]["type"] , __DIR__."/../views/inicidencias_img/", 'views/inicidencias_img/');
	            }

				if(isset($_POST['sol_estado_e'])){
					$estado = $_POST['sol_estado_e'];
				}

				if(isset($_POST['sol_prio_id_e'])){
					$prioridad = $_POST['sol_prio_id_e'];
				}

				if(isset($_POST['sol_ban_id_e'])){
					$banco = $_POST['sol_ban_id_e'];
				}

				if(isset($_POST['sol_suc_id_i_e'])){
					$sucursal = $_POST['sol_suc_id_i_e'];
				}

				$datos = array(
					'sol_suc_id_i' 					=> $sucursal, 
					'sol_requerimiento_t'			=> $_POST['sol_requerimiento_t_e'],
					'sol_observaciones_t'			=> $_POST['sol_observaciones_t_e'],
					'sol_tec_usu_id_i'			    => $tecnico,
					'sol_ban_id_i'					=> $banco,
					'sol_id_i'						=> $_POST['sol_id_i_e'],
					'sol_prio_id'					=> $prioridad,
					'sol_est_id_i'					=> $estado,
					'sol_ruta_evidencia'			=> $ruta,
					'sol_ruta_ot_v'					=> $_POST['sol_ruta_ot_v_e'],
					'sol_id_tps_i'					=> $_POST['sol_id_tps_i_e'],
					'sol_aplica_i'					=> $_POST['sol_aplica_i_e']
				);

				$respuesta = SolicitudesModelo::UpdateDatos($datos);
				if($respuesta == "ok"){
					//Aqui vamos a hacer una validacion de si viene o no la observacion
					if(isset($_POST['sol_observaciones_t_e']) && $_POST['sol_observaciones_t_e'] != ''){
						$datos = array (
							'obs_desc_v'      => $_POST['sol_observaciones_t_e'], //observacion
							'obs_usu_id_i'      => $_SESSION['codigo'],//quien hace la observacion
							'obs_sol_id_i'      => $_POST['sol_id_i_e'], //codigo solicitud
							'obs_fecha_d'      => date('Y-m-d H:i:s') //fecha Observacion
						);
						$observacion = SolicitudesModelo::insertObservaciones($datos);	
					}

					if(isset($_POST['sol_estado_e']) && $_POST['sol_estado_e'] == '5'){
						//Solucionado
						$respuestaX = SolicitudesModelo::mdlEditar('sc_solicitudes', 'sol_fecha_solucion=\''.date('Y-m-d').'\'', 'sol_id_i='.$_POST['sol_id_i_e']); 
					}

					if($tecnico != 0 && $_POST['sol_fecha_cita_d_e'] != '' && $_POST['sol_fecha_cita_d_e'] != 0 ){
						$eliminar = SolicitudesModelo::deleteDatosAsignacion($_POST['sol_id_i_e']);
						$datos = array(
							'asi_usu_tec_id_i' 			=> $tecnico, 
							'asi_fecha_d' 				=> $_POST['sol_fecha_cita_d_e'],
							'asi_hor_id_i'  			=> $_POST['sol_hora_cita_v_e'],
							'asi_observacion_v'		    => $_POST['sol_observaciones_t_e'],
							'asi_sol_id_i'				=> $_POST['sol_id_i_e'],
							'asi_fecha_aignacion'		=> date('Y-m-d')
						);
						$respuesta = SolicitudesModelo::insertDatosAsignar($datos);
					}

					return json_encode(array('code' => 1, 'message' => 'Solicitud actualizada con exito'));
				}else{	
					return json_encode(array('code' => 0, 'message' => 'Solicitud no actualizada'));
				}
			}
		}
		

		/**
		*Desc.  => editar un usuario se solicitan lops campos que llegan por metodo post usu_tip_doc_id_i_i, usu_documento_v_i, usu_per_id_i_i, usu_est_id_i_i, usu_banco_i_i, usu_usuario_v_i, usu_password_v_i, usu_id_i
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/	
		public static function UpdateDatosAsignacion(){
			if(isset($_POST['sol_tec_usu_id_i'])){ //e de edicion
				$eliminar = SolicitudesModelo::deleteDatosAsignacion($_POST['sol_id_i_e']);

				$datos = array(
					'asi_usu_tec_id_i' 			=> $_POST['sol_tec_usu_id_i'], 
					'asi_fecha_d' 				=> $_POST['sol_fecha_cita_d'],
					'asi_hor_id_i'  			=> $_POST['sol_hora_cita_v'],
					'asi_observacion_v'		    => $_POST['sol_observaciones_t_i'],
					'asi_sol_id_i'				=> $_POST['sol_id_i_e'],
					'asi_fecha_aignacion'		=> date('Y-m-d')
				);

				$respuesta = SolicitudesModelo::insertDatosAsignar($datos);
				if($respuesta == "ok"){
					$respuestaX = ModeloAuth:: actualizarUsuarioPostLogin('sc_solicitudes', 'sol_est_id_i', '4', 'sol_id_i', $_POST['sol_id_i_e']);

					//Aqui vamos a hacer una validacion de si viene o no la observacion
					if(isset($_POST['sol_observaciones_t_i']) && $_POST['sol_observaciones_t_i'] != ''){
						$datos = array (
							'obs_desc_v'      => $_POST['sol_observaciones_t_i'], //observacion
							'obs_usu_id_i'      => $_SESSION['codigo'],//quien hace la observacion
							'obs_sol_id_i'      => $_POST['sol_id_i_e'], //codigo solicitud
							'obs_fecha_d'      => date('Y-m-d H:i:s') //fecha Observacion
						);
						$observacion = SolicitudesModelo::insertObservaciones($datos);	
					}

					return json_encode(array('code' => 1, 'message' => 'Solicitud actualizada con exito'));
				}else{	
					return json_encode(array('code' => 0, 'message' => 'Solicitud no actualizada'));
				}
			}
		}

		/**
		*Desc.  => borrar un usuario se solicitan lops campos que llegan por metodo post usu_id_i
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/	
		public static function deleteDatos(){
		    if(isset($_POST['usu_id_i_d'])){ 
			    $datos = $_POST["usu_id_i_d"];
			    $eliminar = SolicitudesModelo::deleteDatosAsignacion($datos);
			    $respuesta = SolicitudesModelo::deleteDatos($datos);
				if($respuesta == "ok"){
					return json_encode(array('code' => 1, 'message' => 'Solicitud Eliminada con exito'));
			    }else{	
					return json_encode(array('code' => 0, 'message' => 'Solicitud no Eliminada'));
				}
			}
		}
		
	}
